package config

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Config struct {
	Server    string `yaml:"server"`
	MongoHost string `yaml:"mongohost"`
	DataBase  string `yaml:"database"`
}

var WebConfig Config

func LoadConfig() {
	file, err := ioutil.ReadFile("config/config.yaml")
	if err != nil {
		log.Fatalf("Read Config File fail %s", err)
	}
	err = yaml.Unmarshal(file, &WebConfig)
}
