package types

import (
	"time"
)

type User struct {
	Id           string `json:"uuid"`
	Email        string `json:"email"`
	PassWord     string `json:"password,omitempty"`
	HashPassWord []byte `json:"hash_password,omitempty"`
}

type Request struct {
	Id        string    `json:"uuid"`
	RequestAt time.Time `json:"request_at,omitempty"`
	KeyWord   string    `json:"keyword,omitempty"`
	//KeyWord 为Main_Page就是访问主页
}

type Response struct {
	Id         string    `json:"uuid"`
	ResponseAt time.Time `json:"response_at,omitempty"`
	KeyWord    string    `json:"keyword,omitempty"`
	Content    string    `json:"content"`
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AuthedUser struct {
	User  User   `json:"user"`
	Token string `json:"token"`
}

type TodayMainPage struct {
	TimeStamp time.Time           `json:"timestamp,string,omitempty"`
	MainPage  map[string][]string `json:"mainpage"`
}

type WikiDoc struct {
	Link      string `json:"link"`
	Keyword   string `json:"keyowrd"`
	Paragraph string `json:"paragraph"`
}

type KeyWordResponse struct {
	Notation string      `json:"notation"`
	KeyWords interface{} `json:"keyword"`
}
