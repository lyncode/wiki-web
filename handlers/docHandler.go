package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"math/rand"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
	"wikiWeb/types"
	"wikiWeb/utils"
)

func GetDoc(w http.ResponseWriter, r *http.Request) {
	var findDoc types.WikiDoc
	vars := mux.Vars(r)
	keyword := vars["keyword"]
	client := <-dbOps.ConnectionPool
	docColl := client.Database(config.WebConfig.DataBase).Collection("Docs")
	findDoc, err := dbOps.FindDoc(docColl, keyword)
	if err != nil {
		utils.DisplayAppError(w, err, "Doc not found", http.StatusInternalServerError)
		dbOps.ConnectionPool <- client
		return
	}
	dbOps.ConnectionPool <- client
	w.Header().Set("Conten-Type", "application/json")
	j, err := json.Marshal(findDoc)
	if err != nil {
		utils.DisplayAppError(w, err, "An unexpected error has occur", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(j)
}

func GetKeyWord(w http.ResponseWriter, r *http.Request) {
	client := <-dbOps.ConnectionPool
	docColl := client.Database(config.WebConfig.DataBase).Collection("Docs")
	keywords, err := dbOps.FindKeyWords(docColl)
	var sliceKeyWords []string
	for _, single := range keywords {
		// interface to string
		sliceKeyWords = append(sliceKeyWords, fmt.Sprint(single))
	}
	length := len(sliceKeyWords)
	var shortKeyWord []string
	for i := 0; i < 10; i++ {
		shortKeyWord = append(shortKeyWord, sliceKeyWords[rand.Intn(length)])
	}

	if err != nil {
		utils.DisplayAppError(w, err, "keyword not found", http.StatusInternalServerError)
		return
	}
	j, err := json.Marshal(types.KeyWordResponse{"keyword", shortKeyWord})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(j)
}
