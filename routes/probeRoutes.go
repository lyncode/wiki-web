package routes

import (
	"github.com/gorilla/mux"
	"wikiWeb/handlers"
)

func SetProbeUser(router *mux.Router) *mux.Router {
	router.HandleFunc("/healthz", handlers.Healthz).Methods("GET")
	router.HandleFunc("/", handlers.Healthz).Methods("GET")
	return router
}
