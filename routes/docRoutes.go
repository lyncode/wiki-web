package routes

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"wikiWeb/handlers"
	"wikiWeb/utils"
)

func SetDocRoutes(router *mux.Router) *mux.Router {
	docRouter := mux.NewRouter()
	docRouter.HandleFunc("/doc/{keyword}", handlers.GetDoc).Methods("GET")
	router.PathPrefix("/doc").Handler(negroni.New(
		negroni.HandlerFunc(utils.Authorize), negroni.Wrap(docRouter)))

	keyRouter := mux.NewRouter()
	keyRouter.HandleFunc("/keyword", handlers.GetKeyWord).Methods("GET")
	router.PathPrefix("/keyword").Handler(negroni.New(
		negroni.HandlerFunc(utils.Authorize), negroni.Wrap(keyRouter)))

	return router
}
