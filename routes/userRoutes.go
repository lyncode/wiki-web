package routes

import (
	"github.com/gorilla/mux"
	"wikiWeb/handlers"
)

func SetUserRoutes(router *mux.Router) *mux.Router {
	router.HandleFunc("/user/register", handlers.Register).Methods("POST")
	router.HandleFunc("/user/login", handlers.Login).Methods("POST")
	return router
}
