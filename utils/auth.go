package utils

import (
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const (
	privKeyPath = "keys/app.rsa"
	pubKeyPath  = "keys/app.rsa.pub"
)

var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

func initKeys() {
	var err error
	signKeyByte, err := ioutil.ReadFile(privKeyPath)
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signKeyByte)
	if err != nil {
		log.Fatalf("[initKeys]signkey: %s\n", err)
	}
	verifyKeyByte, err := ioutil.ReadFile(pubKeyPath)
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyKeyByte)
	if err != nil {
		log.Fatalf("[initKeys]verifykey: %s\n", err)
	}
}
func GenerateJWT(email string) (string, error) {
	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Minute * 2000).Unix(),
		Issuer:    "wikipediaApi",
		Audience:  email,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	tokenString, err := token.SignedString(signKey)
	return tokenString, err
}
func Authorize(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	token, err := jwt.Parse(r.Header.Get("Authorization"), func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})
	if err != nil {
		switch err.(type) {
		case *jwt.ValidationError:
			vErr := err.(*jwt.ValidationError)
			switch vErr.Errors {
			case jwt.ValidationErrorExpired:
				DisplayAppError(w, err,
					"access token expired",
					http.StatusUnauthorized)
				return
			default:
				DisplayAppError(w, err,
					"error while parsing access",
					http.StatusInternalServerError)
				return
			}
		default:
			DisplayAppError(w, err,
				"error while parsing access token",
				http.StatusInternalServerError)
			return
		}
	}
	if token.Valid {
		next(w, r)
	} else {
		DisplayAppError(w, err,
			"Invalid Access Token",
			http.StatusUnauthorized)
	}
}
