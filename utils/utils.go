package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
)

type appError struct {
	Error      string `json:"error"`
	Message    string `json:"message"`
	HttpStatus int    `json:"status"`
}

type errorResource struct {
	Data appError `json:"data"`
}

func DisplayAppError(w http.ResponseWriter, handlerError error, message string, code int) {
	errObj := appError{
		Error:      handlerError.Error(),
		Message:    message,
		HttpStatus: code,
	}
	log.Printf("[AppError]:%s\n", handlerError)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	if j, err := json.Marshal(errorResource{Data: errObj}); err == nil {
		_, _ = w.Write(j)
	}
}

func StartUp() {
	config.LoadConfig()
	initKeys()
	dbOps.InitConnPool()
}
