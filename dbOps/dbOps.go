package dbOps

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
	"log"
	"wikiWeb/types"
)

func Connect(Address string) *mongo.Client {
	clientOptions := options.Client().ApplyURI(Address)
	//clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		func() {
			err = client.Disconnect(context.TODO())
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Connection to MongoDB closed.")
		}()
		log.Fatal(err)
	}
	if err = client.Ping(context.TODO(), nil); err != nil {
		log.Fatal(err)
	} // Check the connection
	fmt.Println("Connected to MongoDB!")
	return client
}

func CreateUser(coll *mongo.Collection, user *types.User) error {
	hashPass, err := bcrypt.GenerateFromPassword(
		[]byte(user.PassWord), bcrypt.DefaultCost,
	)
	if err != nil {
		log.Println("hash password generation fail")
		return err
	}
	user.HashPassWord = hashPass
	user.PassWord = ""

	userUUID, err := uuid.NewUUID()
	if err != nil {
		log.Println("UUID generation fail")
		return err
	}
	user.Id = userUUID.String()

	insertResult, err := coll.InsertOne(context.TODO(), user)
	if err != nil {
		return err
	}
	fmt.Println("Create a new user:", user.Email, insertResult.InsertedID)
	return nil
}

func CheckUser(coll *mongo.Collection, user types.User) (findUser types.User, err error) {
	filter := bson.D{{"email", user.Email}}
	err = coll.FindOne(context.TODO(), filter).Decode(&findUser)
	if err != nil {
		log.Println("Find user fail..")
		return findUser, nil
	}
	if user.Email == findUser.Email {
		err = errors.New("user already exist")
	} else {
		err = nil
	}
	return findUser, err
}

func FindUser(coll *mongo.Collection, user types.Login) (findUser types.User, err error) {
	filter := bson.D{{"email", user.Email}}
	err = coll.FindOne(context.TODO(), filter).Decode(&findUser)
	if err != nil {
		log.Println("Find user fail..")
		return findUser, err
	}
	if user.Email == findUser.Email {
		return findUser, err
	}
	return findUser, errors.New("user not found")
}

func FindDoc(coll *mongo.Collection, keyword string) (findDoc types.WikiDoc, err error) {
	filter := bson.D{{"keyword", keyword}}
	err = coll.FindOne(context.TODO(), filter).Decode(&findDoc)
	if err != nil {
		log.Println("Can not find the doc")
		return findDoc, err
	}
	return findDoc, err
}

func FindKeyWords(coll *mongo.Collection) (findKeyWords []interface{}, err error) {
	findKeyWords, err = coll.Distinct(context.TODO(), "keyword", bson.D{})
	return findKeyWords, nil
}
