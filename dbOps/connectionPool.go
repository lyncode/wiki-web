package dbOps

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"sync"
	"time"
	"wikiWeb/config"
)

const POOL_SIZE = 10

var ConnectionPool chan *mongo.Client
var wg sync.WaitGroup

func getClient(ConnectionPool chan *mongo.Client) {
	wg.Add(1)
	client := Connect(config.WebConfig.MongoHost)
	ConnectionPool <- client
	wg.Done()
}

func InitConnPool() {
	ConnectionPool = make(chan *mongo.Client, POOL_SIZE)
	for i := 0; i < cap(ConnectionPool); i++ {
		go getClient(ConnectionPool)
	}
	wg.Wait()
}

func CheckConnPool() {
	for {
		client := <-ConnectionPool
		log.Println(cap(ConnectionPool), len(ConnectionPool))
		err := client.Ping(context.TODO(), nil)
		if err != nil {
			log.Println(err, "connection expired..")
			newClient := Connect(config.WebConfig.MongoHost)
			ConnectionPool <- newClient
			log.Println(cap(ConnectionPool), len(ConnectionPool))
		}
		log.Println(err, "connection not expired..")
		ConnectionPool <- client
		log.Println(cap(ConnectionPool), len(ConnectionPool))
		time.Sleep(time.Second * 60)
		if len(ConnectionPool) < cap(ConnectionPool) {
			log.Println(cap(ConnectionPool), len(ConnectionPool))
			log.Println("add new connection..")
			newClient := Connect(config.WebConfig.MongoHost)
			ConnectionPool <- newClient
			log.Println(cap(ConnectionPool), len(ConnectionPool))
		}
	}
}
