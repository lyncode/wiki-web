package main

import (
	"github.com/codegangsta/negroni"
	"log"
	"net/http"
	"wikiWeb/config"
	"wikiWeb/dbOps"
	"wikiWeb/routes"
	"wikiWeb/utils"
)

func main() {
	utils.StartUp()
	go dbOps.CheckConnPool()
	router := routes.InitRoutes()
	n := negroni.Classic()
	n.UseHandler(router)
	server := &http.Server{
		Addr:    config.WebConfig.Server,
		Handler: n,
	}
	log.Println("Listening....")
	_ = server.ListenAndServe()

}
